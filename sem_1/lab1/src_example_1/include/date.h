#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class Date {
	private:
		int day, month, year;
		time_t time;

	public:
		Date(int d, int m, int y) {
			if (d<=30 && d >=1) day = d;
			else day = 1;
			
			if (m<=12 && m >=1) month = m;
			else month=1;	
			
			// what's the check for year?	
			year = y;	
		}
		Date(time_t t) {
			struct tm *struct_time = localtime(&t);	
			day = struct_time->tm_mday;
			month = struct_time->tm_mon + 1;
			year = struct_time->tm_year + 1900;
							
		}	
		void print_date() {
			char str_mon[100];
			switch (month) {
				case 1 :
					strcpy(str_mon, "Jan");
				case 2 :
					strcpy(str_mon, "Feb");
				case 3 :
					strcpy(str_mon, "Mar");
				case 4 :
					strcpy(str_mon, "Apr");
				case 5 :
					strcpy(str_mon, "May");
				case 6 :
					strcpy(str_mon, "Jun");
				case 7 :
					strcpy(str_mon, "Jul");
				case 8 :
					strcpy(str_mon, "Aug");
				case 9 :
					strcpy(str_mon, "Sep");
				case 10 :
					strcpy(str_mon, "Oct");
				case 11 :
					strcpy(str_mon, "Nov");
				case 12 :
					strcpy(str_mon, "Dec");

			}			
			printf("Date: %s %d, %d\n",str_mon, day, year); 
		}	
		int get_year() {return year;}
		int get_month() {return month;}
		int get_day() {return day;}
};


void happy_birthday(char* name, Date b);

