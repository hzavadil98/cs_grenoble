#include "date.h"

int main(int argc, char** argv) {
	if (argc != 5) {
		printf("Usage: ./date name, day, month, year\n");
		return -1;
	}	
	Date birthday(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));	
	happy_birthday(argv[1], birthday);		
	return 1;
}
