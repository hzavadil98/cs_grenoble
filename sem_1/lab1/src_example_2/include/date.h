#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class Date {
	private:
		int day, month, year;
		time_t time;

	public:
		Date(int d, int m, int y) {
			if (d<=30 && d >=1) day = d;
			else day = 1;
			
			if (m<=12 && m >=1) month = m;
			else month=1;	
			
			// what's the check for year?	
			year = y;	
		}
		Date(time_t t) {
			struct tm *struct_time = localtime(&t);	
			day = struct_time->tm_mday;
			month = struct_time->tm_mon + 1;
			year = struct_time->tm_year + 1900;
							
		}	
		void print_date() {
			char str_mon[100];
			switch (month) {
				case 1 :
					strcpy(str_mon, "Jan");
					break;
				case 2 :
					strcpy(str_mon, "Feb");
					break;
				case 3 :
					strcpy(str_mon, "Mar");
					break;
				case 4 :
					strcpy(str_mon, "Apr");
					break;
				case 5 :
					strcpy(str_mon, "May");
					break;
				case 6 :
					strcpy(str_mon, "Jun");
					break;
				case 7 :
					strcpy(str_mon, "Jul");
					break;
				case 8 :
					strcpy(str_mon, "Aug");
					break;
				case 9 :
					strcpy(str_mon, "Sep");
					break;
				case 10 :
					strcpy(str_mon, "Oct");
					break;
				case 11 :
					strcpy(str_mon, "Nov");
					break;
				case 12 :
					strcpy(str_mon, "Dec");
					break;
			}			
			printf("%s %d, %d\n",str_mon, day, year); 
		}	
		int difference(Date* d2) {
			return 365*(this->year-d2->year)
				+ 30*(this->month-d2->month)
				+ this->day - d2->day;
		}
		bool before(Date* d2) {
			int d = this->difference(d2);
			if (d<0) return true;
			return false;
		}
		int duration(Date* d2) { return this->difference(d2);}
};
