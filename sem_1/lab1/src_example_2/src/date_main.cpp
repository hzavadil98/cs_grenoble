#include "trip.h"

int main(int argc, char** argv) {
	if (argc != 7) {
		printf("Usage: ./date start_day start_month start_year end_day end_month end_year\n");
		return -1;
	}	
		
	Date start(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));	
	Date end(atoi(argv[4]), atoi(argv[5]), atoi(argv[6]));	
	if (end.before(&start)) {printf("you can't start a trip after you've ended the trip bro.\n"); return -1;}
	int price;	
	printf("Input price of trip\n");
	scanf("%d", &price);	
	Trip t(&start, &end, price);
	t.print_trip();	
	return 1;
}
