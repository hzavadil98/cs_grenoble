#include "../include/classes/date.hpp"
#include "../include/classes/trip.hpp"
#include "../include/others/utils.hpp"


int main(int argc, char** argv) {
    //if (argc != 5) return -1;

    Date debut;
    char wd[100] = "Monday";
    Date beginning(30, 8, 2021, wd);
    Date end(30, 9, 2022, wd);
    Date tmp(end);
    
    std::cout << beginning << std::endl;
    std::cout << end << std::endl;
    std::cout << "Duration: " << duration(beginning, end) << std::endl;

    return 0;
}