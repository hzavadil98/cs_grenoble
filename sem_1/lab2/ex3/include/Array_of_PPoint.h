#ifndef ARRAY_OF_PPOINT_H
#define ARRAY_OF_PPOINT_H

// Includes
#include "PPoint.h"

using namespace std;

// shortcut
typedef unsigned int uint;

/* 
 * \file Array_of_PPoint.h
 * \brief define class for array of ppoints
 */

// Array of PPoint
//  * hold an array of PPoint with some length
//  * have a printer, constructor, and destructor
class Array_of_PPoint {
		uint length;
		PPoint *arr;
	public:
		/* \fn Array_of_PPoint(uint len)
		 * \brief allocate a (random) array of ppoint with length len
		 * \param unsigned length of array
		 * \return none
		 */
		Array_of_PPoint(uint);
		/*
		 * \fn ~Array_of_PPoint()
		 * \brief deallocate array
		 * \param none
		 * \return none
		 */
		~Array_of_PPoint();
		/*
		 * \fn void print_tab() const
		 * \brief print with tabs
		 * \param nothing
		 * \return nothing (prints to screen)
		 */	
		void print_tab() const;
		/*
		 * \fn void add(PPoint &p)
		 * \brief add two points together
		 * \param ppoint
		 * \return nothing
		 */			
		void add(PPoint &p);
};
#endif
