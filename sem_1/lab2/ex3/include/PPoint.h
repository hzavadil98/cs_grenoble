#ifndef PPOINT_H
#define PPOINT_H

// Includes
#include <stdio.h>
#include <stdlib.h>

using namespace std;

// shortcut
typedef unsigned int uint;

/*
 * \file PPoint.h
 * \brief deine a PPoint class to hold an x and y
*/

// PPoint
//  * hold 2 ints and have setters, printing, and constructors/destrcutors
class PPoint{
	private:
		int *x, *y;
	public:
		/*
		 * \fn PPoint()
		 * \brief create a point without setting x or y
		 * \param none
		 * \return none
		 */	
		PPoint() {this->x = new int; this->y = new int; }
		/*
		 * \fn PPoint(int x, int y)
		 * \brief create a point and set x=a and y=b
		 * \param int a and int b (coordinates) 
		 * \return none
		 */	
		PPoint(int, int);
		/*
		 * \fn PPoint(const PPoint &p)
		 * \brief create a point and copy p's coordinates
		 * \param reference point to copy without modifying
		 * \return none
		 */	
		PPoint(const PPoint &p);	
		/*
		 * \fn ~PPoint()
		 * \brief deallocate point
		 * \param none
		 * \return none
		 */	
		~PPoint();
		/*
		 * \fn add(PPoint *p)
		 * \brief add two points together component wise
		 * \param pointer to PPoint to be added
		 * \return none	
	       	*/	
		void add(PPoint *p);
                /*
                 * \fn set_coords(int a, int b)
                 * \brief set coordinates x=a, y=b
                 * \param int a and b
                 * \return none 
                */     			
		void set_coords(int, int);		
                /*
                 * \fn print() const
                 * \brief print out point
                 * \param none
                 * \return none 
                */     			
		void print() const;
};
#endif
