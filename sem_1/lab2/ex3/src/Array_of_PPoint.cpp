// Includes
#include "Array_of_PPoint.h"
#include "PPoint.h"

using namespace std;

// shortcut
typedef unsigned int uint;

// Array of PPoint
//  * hold an array of PPoint with some length
//  * have a printer, constructor, and destructor
Array_of_PPoint::~Array_of_PPoint() {
	printf("Calling destructor for Array_of_PPoint\n");
	delete[] arr;
}
Array_of_PPoint::Array_of_PPoint(uint len) {
	printf("Calling constructor for Array_of_PPoint\n");
	this->length = len;	
	this->arr = new PPoint[len];
	for (int i=0;i<(int)len;i++)
		this->arr[i].set_coords(rand()%6, rand()%6);

}
void Array_of_PPoint::add(PPoint &p) {
	for (int i=0;i<(int)this->length;i++)
		try 
			{this->arr[i].add(&p);}
		catch (const char* msg)
			{printf("error: %s. skipping element %d\n", msg, i);}
}

void Array_of_PPoint::print_tab() const {
	for (int i=0;i<(int)this->length;i++)
	{printf("\t"); this->arr[i].print();}
}

