// Includes
#include "Array_of_PPoint.h"
#include "PPoint.h"

using namespace std;

int main(int argc, char** argv) {
	Array_of_PPoint a(10);
	printf("before adding (2, 16): \n");
	a.print_tab();	
	PPoint p(2,16);
	a.add(p);
	printf("after adding (2, 16): \n");
	a.print_tab();	
	return 1;
}
